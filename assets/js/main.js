var navLinks = document.getElementById("navLinks");
showMenu = () => (navLinks.style.display = "block");
hideMenu = () => (navLinks.style.display = "none");

var navbar = document.getElementById("navbar");
var sticky = 500;

const stickNavBarToTop = () => {
  if (window.pageYOffset >= sticky) {
    document.getElementById("logoText").src = "assets/img/UNEQTYtextwhite.svg";
    navbar.classList.add("sticky");
  } else {
    document.getElementById("logoText").src = "assets/img/UNEQTYlogo.svg";
    navbar.classList.remove("sticky");
  }
};

window.onscroll = () => {
  stickNavBarToTop();
};

function showContent(evt, cityName) {
  evt.preventDefault();
  var i, tabcontent, tabbuttons;
  tabcontent = document.getElementsByClassName("step-container");
  tabbuttons = document.getElementsByClassName("capsule-button");
  console.log(tabbuttons, "tabbuttons");
  if (cityName === "investor") {
    console.log("Investor");
    tabcontent[0].style.display = "flex";
    tabcontent[1].style.display = "none";
    if (!tabbuttons[0].classList.contains("active")) {
      tabbuttons[0].classList.add("active");
      tabbuttons[1].classList.remove("active");
    }
  }
  if (cityName === "shareholder") {
    console.log("shareholder");
    tabcontent[0].style.display = "none";
    tabcontent[1].style.display = "flex";
    if (!tabbuttons[1].classList.contains("active")) {
      tabbuttons[1].classList.add("active");
      tabbuttons[0].classList.remove("active");
    }
  }
}
